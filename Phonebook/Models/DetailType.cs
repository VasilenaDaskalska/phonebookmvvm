﻿namespace Phonebook.Models
{
    public class DetailType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}
