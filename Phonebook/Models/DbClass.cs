﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Phonebook.Models
{
    public class DbClass
    {
        //Connecting to DB
        static string connectonString = "Server=localhost; Port=3306; Database=phonebook; Uid=root; Pwd=root";
        MySqlConnection connection = new MySqlConnection(connectonString);

        //Getting all contacts from DB.
        //Returing List.
        public List<Contact> GetContacts()
        {
            List<Contact> contacts = new List<Contact>();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM contacts";
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                Contact contact = null;

                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        contact = new Contact();
                        contact.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        contact.LastName = reader.GetString(reader.GetOrdinal("Last_Name"));
                        contact.Name = reader.GetString(reader.GetOrdinal("Name"));
                        contacts.Add(contact);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return contacts;
        }

        //Getting all detail types from DB.
        //Returing List.
        public List<DetailType> GetDetailsType()
        {
            List<DetailType> detailTypes = new List<DetailType>();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM details_type";
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    DetailType detailType = null;
                    while (reader.Read())
                    {
                        detailType = new DetailType();
                        detailType.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        detailType.TypeName = reader.GetString(reader.GetOrdinal("TypeName"));
                        detailTypes.Add(detailType);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return detailTypes;
        }

        //Getting all details from DB.
        //Returning List.
        public List<Detail> GetDetails()
        {
            List<Detail> details = new List<Detail>();
            Detail detail = null;
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM details";
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        detail = new Detail();
                        detail.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        detail.TypeId = reader.GetInt32(reader.GetOrdinal("TypeId"));
                        detail.Value = reader.GetString(reader.GetOrdinal("Value"));
                        detail.ContactId = reader.GetInt32(reader.GetOrdinal("ContactId"));
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return details;
        }

        //Inserting contacts in Contacts table.
        public bool InsertIntoContactsTable(Contact contact)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "INSERT INTO contacts(Name,Last_Name)  VALUES (@Name , @Last_Name)";
                MySqlCommand command = new MySqlCommand(sql, connection);

                command.Parameters.AddWithValue("Id", contact.Id);
                command.Parameters.AddWithValue("Name", contact.Name);
                command.Parameters.AddWithValue("Last_Name", contact.LastName);

                connection.Open();
                IsSuccess = command.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }
            return IsSuccess;
        }

        //Inserting contacts in Details table.
        public bool InsertIntoDetailsTable(Detail detail)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);

            try
            {
                string sql = "INSERT INTO details(TypeId, Value , ContactId)  VALUES (@TypeId , @Value , @ContactId)";
                MySqlCommand command = new MySqlCommand(sql, connection);
                command.Parameters.AddWithValue("TypeId", detail.TypeId);
                command.Parameters.AddWithValue("Value", detail.Value);
                command.Parameters.AddWithValue("ContactId", detail.ContactId);
                connection.Open();
                IsSuccess = command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                connection.Close();
            }
            return IsSuccess;
        }

        //Updating Contact.
        public bool UpdateContact(Contact contact)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);

            try
            {
                string sql = "UPDATE contacts SET Name = @Name, Last_Name = @Last_Name WHERE Id = @Id";
                MySqlCommand command = new MySqlCommand(sql, connection);
                connection.Open();
                command.Parameters.AddWithValue("@Id", contact.Id);
                command.Parameters.AddWithValue("@Name", contact.Name);
                command.Parameters.AddWithValue("@Last_Name", contact.LastName);
                IsSuccess = command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;
        }

        //Updating Detail.
        public bool UpdateDetail(Detail detail)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);

            try
            {
                string sql = "UPDATE details SET TypeId = @TypeId, Value = @Value,ContactId =@ContactId WHERE Id =@Id";
                MySqlCommand command = new MySqlCommand(sql, connection);
                connection.Open();
                command.Parameters.AddWithValue("@Id", detail.Id);
                command.Parameters.AddWithValue("@Value", detail.Value);
                command.Parameters.AddWithValue("@ContactId", detail.ContactId);
                command.Parameters.AddWithValue("@TypeId", detail.TypeId);
                IsSuccess = command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;
        }

        //Deleting contact by Id.
        public bool DeleteContact(int Id)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                MySqlCommand commandForDeletingContact = new MySqlCommand("delete from contacts where contacts.Id =" + Id, connection);
                connection.Open();
                IsSuccess = commandForDeletingContact.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;

        }

        //Deleting detail by Id.
        public bool DeleteDetail(int Id)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                MySqlCommand commandForDeletingDetail = new MySqlCommand("delete from details where Id = " + Id + " ", connection);
                connection.Open();
                IsSuccess = commandForDeletingDetail.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;

        }

        //Deleting contact by ContactId.
        public bool DeleteDetailByContactid(int ContactId)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                MySqlCommand commandForDeletingDetail = new MySqlCommand("delete from details where ContactId = " + ContactId + " ", connection);
                connection.Open();
                IsSuccess = commandForDeletingDetail.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;

        }

        //Serching Detail by contact Id.
        //Returning List.
        public List<Detail> SearchDetailByContactIdReturingList(int ContactId)
        {
            List<Detail> result = null;

            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT details.Id , details.Value, phonebook.details_type.TypeName  FROM details INNER JOIN  phonebook.details_type on phonebook.details.TypeId = phonebook.details_type.Id  where ContactId =" + ContactId;
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (result == null)
                        {
                            result = new List<Detail>();
                        }

                        var detail = new Detail();
                        detail.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        detail.Value = reader.GetString(reader.GetOrdinal("Value"));
                        detail.TypeName = reader.GetString(reader.GetOrdinal("TypeName"));

                        result.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        //Searching Contact by name or part of name.
        //Returing List.
        public List<Contact> SearchContactByName(string name)
        {
            List<Contact> contacts = new List<Contact>();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT phonebook.contacts.Id,phonebook.contacts.Name, phonebook.contacts.Last_Name From phonebook.contacts  where contacts.Name Like '%" + name + "%'";
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                Contact contact = null;

                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        contact = new Contact();
                        contact.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        contact.LastName = reader.GetString(reader.GetOrdinal("Last_Name"));
                        contact.Name = reader.GetString(reader.GetOrdinal("Name"));
                        contacts.Add(contact);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return contacts;
        }

        //Searching Contact By Id.
        //Returning object.
        public Contact SearchContactById(int id)
        {
            Contact contact = new Contact();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT phonebook.contacts.Id,phonebook.contacts.Name, phonebook.contacts.Last_Name From phonebook.contacts " +
                         "where contacts.Id = " + id;
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        contact = new Contact();
                        contact.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        contact.LastName = reader.GetString(reader.GetOrdinal("Last_Name"));
                        contact.Name = reader.GetString(reader.GetOrdinal("Name"));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return contact;
        }

        //Searching Detail by Id.
        //Returning object.
        public Detail SearchDetailById(int id)
        {
            Detail detail = new Detail();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT details.Id , details.Value, phonebook.details_type.TypeName  FROM details INNER JOIN  phonebook.details_type on phonebook.details.TypeId = phonebook.details_type.Id  where details.Id =" + id;
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        detail.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        detail.Value = reader.GetString(reader.GetOrdinal("Value"));
                        detail.TypeName = reader.GetString(reader.GetOrdinal("TypeName"));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return detail;
        }

        //Getting the Id from last added Contact.
        public int GetLastAddedIndex()
        {
            string connectonString = "Server=localhost; Port=3306; Database=phonebook; Uid=root; Pwd=root;";
            MySqlConnection connection;
            connection = new MySqlConnection(connectonString);
            string sql2 = "SELECT LAST_INSERT_ID()";
            MySqlCommand command1 = new MySqlCommand(sql2, connection);
            connection.Open();
            MySqlDataReader mySqlDataReader = command1.ExecuteReader();
            int lastAddedId = 0;
            while (mySqlDataReader.Read())
            {
                lastAddedId = Int32.Parse(mySqlDataReader[0].ToString());
            }

            return lastAddedId;
        }

        //Validation for requiredField.
        public bool RequiredField(string text)
        {
            bool IsValid = false;
            if (!text.Equals(string.Empty))
            {
                IsValid = true;
            }

            return IsValid;
        }

        //Validation for Email.
        public bool EmailValidator(string email)
        {
            bool isValid = false;
            if (RequiredField(email))
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                if (match.Success)
                {
                    isValid = true;
                }
            }
            return isValid;
        }

        //Validation for number.
        public bool NumberValidator(string number)
        {
            bool isValid = false;
            if (RequiredField(number))
            {
                if (number.Length >= 10 && number.Length <= 15)
                {
                    if (Int32.TryParse(number, out int chacker))
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }
    }
}
