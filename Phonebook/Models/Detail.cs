﻿namespace Phonebook.Models
{
    public class Detail
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int ContactId { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }
}
