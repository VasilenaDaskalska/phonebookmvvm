﻿using Phonebook.Commands;
using Phonebook.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Phonebook.ViewModels
{
    /// <summary>
    /// Making all logic for MainWindow
    /// </summary>
    public class UpdateViewModel : BaseViewModel
    {
        #region Private properties
        //Instances of the classes
        private DbClass dbClass;
        private Contact contactClass;
        private Detail detailClass;

        //Needed properties from model
        private string name = string.Empty;
        private string lastName = string.Empty;
        private string detail = string.Empty;

        //List with all details from Detail class
        private List<Detail> details;

        //List with all details from Contact class
        private List<Contact> contacts;

        //Needed properties for methods
        private string errorStringMessage;
        private int selectedDetailType;
        private int selectedItem;
        private int selectedDetailId;

        #endregion

        #region Private commands
        private ICommand selectedContactChangedCommand;
        private ICommand selectedDetailChangedCommand;
        private ICommand saveCommand;
        private ICommand clearCommand;
        private ICommand closeCommand;
        #endregion

        #region Constructor
        public UpdateViewModel()
        {
            //Initialization of the class
            this.dbClass = new DbClass();
            this.contactClass = new Contact();
            this.detailClass = new Detail();

            //Filling the list with all contacts
            this.Contacts = dbClass.GetContacts();

            //Filling combobox with DetailTypes
            this.DetailTypesCombo = dbClass.GetDetailsType();
        }
        #endregion

        #region Public proterties
        //Making list that will contains DetailTypes
        public List<DetailType> DetailTypesCombo { get; }

        //Initialization of all private properties

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                OnPropertyChanged(nameof(this.Name));
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }

            set
            {
                this.lastName = value;
                OnPropertyChanged(nameof(this.LastName));
            }
        }

        public string Detail
        {
            get
            {
                return this.detail;
            }

            set
            {
                this.detail = value;
                OnPropertyChanged(nameof(this.Detail));
            }
        }

        public List<Detail> Details
        {
            get { return this.details; }
            set
            {
                this.details = value;
                this.OnPropertyChanged(nameof(this.Details));
            }
        }

        public List<Contact> Contacts
        {
            get { return this.contacts; }

            set
            {
                this.contacts = value;
                this.OnPropertyChanged(nameof(this.Contacts));
            }
        }

        public string ErrorStringMessage
        {
            get
            {
                return this.errorStringMessage;
            }

            set
            {
                this.errorStringMessage = value;
                OnPropertyChanged(nameof(this.ErrorStringMessage));
            }
        }

        public int SelectedDetailType
        {
            get
            {
                return this.selectedDetailType;
            }
            set
            {
                this.selectedDetailType = value;
                OnPropertyChanged(nameof(this.SelectedDetailType));
            }
        }

        public int SelectedItem
        {
            get
            {
                return this.selectedItem;
            }
            set
            {
                this.selectedItem = value;
                OnPropertyChanged(nameof(this.SelectedItem));
            }
        }

        public int SelectedDetailId
        {
            get
            {
                return this.selectedDetailId;
            }
            set
            {
                this.selectedDetailId = value;
                OnPropertyChanged(nameof(this.SelectedDetailId));
            }
        }

        #endregion

        #region Public Commands
        public ICommand SelectedContactChangedCommand
        {
            get
            {
                if (this.selectedContactChangedCommand == null)
                {
                    this.selectedContactChangedCommand = new RelayCommand<object>(this.SelectedContactChanged);
                }

                return this.selectedContactChangedCommand;
            }
        }

        public ICommand SelectedDetailChangedCommand
        {
            get
            {
                if (this.selectedDetailChangedCommand == null)
                {
                    this.selectedDetailChangedCommand = new RelayCommand<object>(this.SelectedDetailChanged);
                }

                return this.selectedDetailChangedCommand;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new RelayCommand<object>(this.Save, this.CanSave);
                }

                return this.saveCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (this.clearCommand == null)
                {
                    this.clearCommand = new RelayCommand<object>(this.Clear);
                }

                return this.clearCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand<Window>(this.Close);
                }

                return this.closeCommand;
            }
        }
        #endregion

        #region Private Methods
        //Can exdecute method for SaveCommand
        private bool CanSave(object args)
        {
            if (dbClass.RequiredField(Name) && dbClass.RequiredField(Detail))
            {

                return true;
            }
            else
            {
                ErrorStringMessage = "Field is required!";
                return false;
            }
        }

        //When one contact is clecked from the ListView all details apearse in details ListView
        private void SelectedContactChanged(object args)
        {
            this.Details = dbClass.SearchDetailByContactIdReturingList(selectedItem);
            this.contactClass = dbClass.SearchContactById(selectedItem);
            this.Name = contactClass.Name;
            this.LastName = contactClass.LastName;

        }

        //When add or delete detail it gets the count of all details
        private void SelectedDetailChanged(object args)
        {
            detailClass = dbClass.SearchDetailById(selectedDetailId);
            this.Detail = detailClass.Value;
        }

        //Save method
        private void Save(object args)
        {
            contactClass.Name = this.name;
            contactClass.LastName = this.lastName;
            detailClass.Value = this.Detail;
            detailClass.TypeId = this.selectedDetailType;
            detailClass.ContactId = this.selectedItem;

            if (SelectedDetailType == 4)
            {
                if (!dbClass.EmailValidator(Detail))
                {
                    MessageBox.Show("Please enter correct e-mail!");
                    return;
                }
            }

            if (SelectedDetailType == 1 || SelectedDetailType == 2)
            {
                if (!dbClass.NumberValidator(Detail))
                {

                    MessageBox.Show("Please enter correct number!");
                    return;
                }
            }

            bool isSuccessUpdatedContact = dbClass.UpdateContact(contactClass);
            bool isSuccessUpdatedDetails = dbClass.UpdateDetail(detailClass);


            if (isSuccessUpdatedContact && isSuccessUpdatedDetails)
            {
                MessageBox.Show("Contact successful updated!");
                Clear(args);
            }
            else
            {
                MessageBox.Show("Failed updating contact!");
            }
        }

        //Clearing all TextBoxes when the button is clicked
        private void Clear(object args)
        {
            this.Name = string.Empty;
            this.LastName = string.Empty;
            this.Detail = string.Empty;
        }

        //Closing InsertWindow when the button is clicked
        private void Close(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }
        #endregion
    }
}
