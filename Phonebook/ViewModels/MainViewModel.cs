﻿using Phonebook.Commands;
using Phonebook.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Phonebook.ViewModels
{
    /// <summary>
    /// Making all logic for MaintWindow
    /// </summary>
    public class MainViewModel : BaseViewModel
    {
        #region Private properties
        //Instance of the classe
        private readonly DbClass dbClass;

        //List with all details from Detail class
        private List<Detail> details;

        //List with all contacts from Contact class
        private List<Contact> contacts;

        //Needed properties for methods
        private string searchedName;
        private int selectedItem;
        private int selectedDetailId;
        private int rowCount;
        #endregion

        #region Private commands
        private ICommand selectedContactChangedCommand;
        private ICommand selectedDetailChangedCommand;
        private ICommand searchContactByNameCommand;
        private ICommand detelteContactCommand;
        private ICommand deleteDetailCommand;
        private ICommand refreshCommand;
        private ICommand openInsertWindowCommand;
        private ICommand openUpdateWindowCommand;
        private ICommand openMoreDeailsWindowCommand;
        #endregion

        #region Constructor
        public MainViewModel()
        {
            //Initialization of the class
            this.dbClass = new DbClass();

            //Filling the list with all contacts
            Contacts = dbClass.GetContacts();
        }
        #endregion

        #region Public proeprties
        //Initialization of all private properties
        public List<Contact> Contacts
        {
            get { return this.contacts; }

            set
            {
                this.contacts = value;
                this.OnPropertyChanged(nameof(this.Contacts));
            }
        }

        public List<Detail> Details
        {
            get { return this.details; }
            set
            {
                this.details = value;
                this.OnPropertyChanged(nameof(this.Details));
            }
        }

        public string SearchedName
        {
            get { return this.searchedName; }
            set
            {
                this.searchedName = value;
                this.OnPropertyChanged(nameof(this.SearchedName));
            }
        }

        public int SelectedItem
        {
            get
            {
                return this.selectedItem;
            }
            set
            {
                this.selectedItem = value;
                OnPropertyChanged(nameof(this.SelectedItem));
            }
        }

        public int SelectedDetailId
        {
            get
            {
                return this.selectedDetailId;
            }
            set
            {
                this.selectedDetailId = value;
                OnPropertyChanged(nameof(this.SelectedDetailId));
            }
        }
        #endregion

        #region Public Commands
        public ICommand SelectedContactChangedCommand
        {
            get
            {
                if (this.selectedContactChangedCommand == null)
                {
                    this.selectedContactChangedCommand = new RelayCommand<object>(this.SelectedContactChanged);
                }

                return this.selectedContactChangedCommand;
            }
        }

        public ICommand SelectedDetailChangedCommand
        {
            get
            {
                if (this.selectedDetailChangedCommand == null)
                {
                    this.selectedDetailChangedCommand = new RelayCommand<object>(this.SelectedDetailChanged);
                }

                return this.selectedDetailChangedCommand;
            }
        }

        public ICommand SearchContactByNameCommand
        {
            get
            {
                if (this.searchContactByNameCommand == null)
                {
                    this.searchContactByNameCommand = new RelayCommand<object>(this.SearchContactByName);
                }

                return this.searchContactByNameCommand;
            }
        }

        public ICommand DetelteContactCommand
        {
            get
            {
                if (this.detelteContactCommand == null)
                {
                    this.detelteContactCommand = new RelayCommand<object>(this.DeleteContact, this.CanDeleteContact);
                }

                return this.detelteContactCommand;
            }
        }

        public ICommand DeleteDetailCommand
        {
            get
            {
                if (this.deleteDetailCommand == null)
                {
                    this.deleteDetailCommand = new RelayCommand<object>(this.DeleteDetail , this.CanDeleteDetail);
                }

                return this.deleteDetailCommand;
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                if (this.refreshCommand == null)
                {
                    this.refreshCommand = new RelayCommand<object>(this.UpdateData);
                }
                return this.refreshCommand;
            }
        }

        public ICommand OpenInsertWindowCommand
        {
            get
            {
                if (this.openInsertWindowCommand == null)
                {
                    this.openInsertWindowCommand = new RelayCommand<object>(this.OpenInserWindow);
                }

                return this.openInsertWindowCommand;
            }
        }

        public ICommand OpenUpdateWindowCommand
        { 
            get
            {
                if (this.openUpdateWindowCommand == null)
                {
                    this.openUpdateWindowCommand = new RelayCommand<object>(this.OpenUpdateWindow);
                }

                return this.openUpdateWindowCommand;
            }
        }

        public ICommand OpenMoreDeailsWindowCommand
        {
            get
            {
                if (this.openMoreDeailsWindowCommand == null)
                {
                    this.openMoreDeailsWindowCommand = new RelayCommand<object>(this.OpenMoreDetailsWindow);
                }

                return this.openMoreDeailsWindowCommand;
            }
        }
        #endregion

        #region Private Methods

        //Can exdecute method for DeleteContactCommand
        private bool CanDeleteContact(object args)
        {
            if(contacts == null)
            {
                return false;
            }

            return true;
        }

        //Can exdecute method for DeleteDetailCommand
        private bool CanDeleteDetail(object args)
        {
            if (rowCount <= 1)
            {
                return false;
            }
            return true;
        }

        //When one contact is clecked from the ListView all details apearse in details ListView
        private void SelectedContactChanged(object args)
        {
            //Filling the list with all details that ContactId is equal with Id of the clicked contact
            this.Details = dbClass.SearchDetailByContactIdReturingList(selectedItem);
        }

        //When add or delete detail it gets the count of all details
        private void SelectedDetailChanged(object args)
        {
            if (details == null)
            {
                rowCount = 0;
            }
            else
            {
                rowCount = details.Count;
            }
        }

        //Serching contact bu name or part of a name
        private void SearchContactByName(object args)
        {
            //Filling the contact list only with contacts that contains the searched name or part of a name
            this.Contacts = dbClass.SearchContactByName(this.searchedName);
        }

        //Deleting whole contact with all details for it when the button is clicked
        private void DeleteContact(object args)
        {

            bool IsDeleteDetailSuccess = dbClass.DeleteDetailByContactid(selectedItem);
            bool IsDeleteContactsSuccess = dbClass.DeleteContact(selectedItem);

            if (IsDeleteContactsSuccess && IsDeleteDetailSuccess)
            {
                MessageBox.Show("Contact is deleted successfull!");
                this.UpdateData(args);
            }
            else
            {
                MessageBox.Show("Cannot delete contact!");
            }
        }

        //Deleting detail for selected contact
        private void DeleteDetail(object args)
        {
            bool isDeleted = dbClass.DeleteDetail(selectedDetailId);
            if (isDeleted)
            {
                MessageBox.Show("Detail is deleted successfull!");
            }
            else
            {
                MessageBox.Show("Cannot delete detail!");
            }
        }

        //Updating Contact ListView  when the refresh button is clicked 
        private void UpdateData(object args)
        {
            this.Contacts = dbClass.GetContacts();
        }

        //Open InsertWindow when the button is clicked
        private void OpenInserWindow(object args)
        {
            Insert insert = new Insert();
            insert.Show();
        }

        //Open UpdatetWindow when the button is clicked
        private void OpenUpdateWindow(object args)
        {
            UpdateWindow update = new UpdateWindow();
            update.Show();
        }

        //Open MoreDetailtWindow when the button is clicked
        private void OpenMoreDetailsWindow(object args)
        {
            MoreDetails moreDetails = new MoreDetails();
            moreDetails.Show();
        }
        #endregion
    }
}