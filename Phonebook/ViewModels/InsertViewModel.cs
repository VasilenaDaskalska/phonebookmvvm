﻿using Phonebook.Commands;
using Phonebook.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Phonebook.ViewModels
{
    /// <summary>
    /// Making all logic for InsertWindow
    /// </summary>
    public class InsertViewModel : BaseViewModel
    {
        #region Private properties
        //Instances of the classes
        private readonly DbClass dbClass;
        private readonly Contact contactClass;
        private readonly Detail detailClass;

        //Needed properties from model
        private string name = string.Empty;
        private string lastName = string.Empty;
        private string detail = string.Empty;

        //Needed properties for methods
        private int lastAddedContactId;
        private int selectedDetailType;
        private string errorStringMessage;
        #endregion

        #region Private commands
        private ICommand saveCommand;
        private ICommand clearCommand;
        private ICommand closeCommand;
        #endregion

        #region Constructor
        public InsertViewModel()
        {
            //Initialization of the classes
            this.dbClass = new DbClass();
            this.contactClass = new Contact();
            this.detailClass = new Detail();

            //Filling combobox with DetailTypes
            this.DetailTypesCombo = dbClass.GetDetailsType();
        }
        #endregion

        #region Public properties

        //Making list that will contains DetailTypes
        public List<DetailType> DetailTypesCombo { get; }

        //Initialization of all private properties
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                OnPropertyChanged(nameof(this.Name));
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }

            set
            {
                this.lastName = value;
                OnPropertyChanged(nameof(this.LastName));
            }
        }

        public string Details
        {
            get
            {
                return this.detail;
            }

            set
            {
                this.detail = value;
                OnPropertyChanged(nameof(this.Details));
            }
        }

        public int SelectedDetailType
        {
            get
            {
                return this.selectedDetailType;
            }
            set
            {
                this.selectedDetailType = value;
                OnPropertyChanged(nameof(this.SelectedDetailType));
            }
        }

        public string ErrorStringMessage
        {
            get
            {
                return this.errorStringMessage;
            }

            set
            {
                this.errorStringMessage = value;
                OnPropertyChanged(nameof(this.ErrorStringMessage));
            }
        }
        #endregion

        #region Public Comands
        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new RelayCommand<object>(this.Save, this.CanSave);
                }

                return this.saveCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (this.clearCommand == null)
                {
                    this.clearCommand = new RelayCommand<object>(this.Clear);
                }

                return this.clearCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand<Window>(this.Close);
                }

                return this.closeCommand;
            }
        }
        #endregion

        #region Private Methods

        //Can exdecute method for SaveCommand
        private bool CanSave(object args)
        {
            if (dbClass.RequiredField(Name) && dbClass.RequiredField(Details))
            {

                return true;
            }
            else
            {
                ErrorStringMessage = "Field is required!";
                return false;
            }
        }

        //Save method
        private void Save(object args)
        {
            this.contactClass.Name = this.Name;
            this.contactClass.LastName = this.LastName;
            this.detailClass.Value = this.detail;
            this.detailClass.TypeId = this.selectedDetailType;

            if (SelectedDetailType == 4)
            {
                if (!dbClass.EmailValidator(Details))
                {
                    MessageBox.Show("Please enter correct e-mail!");
                    return;

                }
            }

            if (SelectedDetailType == 1 || SelectedDetailType == 2)
            {
                if (!dbClass.NumberValidator(Details))
                {
                    MessageBox.Show("Please enter correct number!");
                    return;
                }
            }

            bool isSuccessAddedContact = this.dbClass.InsertIntoContactsTable(this.contactClass);
            this.lastAddedContactId = this.dbClass.GetLastAddedIndex();
            this.detailClass.ContactId = this.lastAddedContactId;
            bool isSuccessAddedDetail = this.dbClass.InsertIntoDetailsTable(this.detailClass);

            if (isSuccessAddedContact && isSuccessAddedDetail)
            {
                MessageBox.Show("New contact successful added!");
                Clear(args);
            }
            else
            {
                MessageBox.Show("Failed adding contact!");
            }
        }

        //Clearing all TextBoxes when the button is clicked
        private void Clear(object args)
        {
            this.Name = string.Empty;
            this.LastName = string.Empty;
            this.Details = string.Empty;
        }

        //Closing InsertWindow when the button is clicked
        private void Close(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }
        #endregion

    }
}