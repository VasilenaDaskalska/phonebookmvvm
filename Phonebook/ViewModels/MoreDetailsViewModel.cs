﻿using Phonebook.Commands;
using Phonebook.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Phonebook.ViewModels
{
    /// <summary>
    /// Making all logic for MoreDetailsWindow
    /// </summary>
    public class MoreDetailsViewModel : BaseViewModel
    {
        #region Private properties
        //Instances of the classes
        private readonly DbClass dbClass;
        private readonly Detail detailClass;

        //Needed properties from model
        private string details = string.Empty;

        //Needed properties for methods
        private int selectedDetailType;
        private int selectedContactId;
        private string errorMessage = string.Empty;
        #endregion

        #region Private commands
        private ICommand saveCommand;
        private ICommand clearCommand;
        private ICommand closeCommand;
        #endregion

        #region Constructor
        public MoreDetailsViewModel()
        {
            //Initialization of the classes
            this.dbClass = new DbClass();
            this.detailClass = new Detail();

            //Filling combobox with DetailTypes
            DetailTypesCombo = dbClass.GetDetailsType();

            //Filling combobox with Contacts
            ContactsCombo = dbClass.GetContacts();
        }
        #endregion

        #region Public properties
        //Making list that will contains DetailTypes
        public List<DetailType> DetailTypesCombo { get; }

        //Making list that will contains Contacts
        public List<Contact> ContactsCombo { get; }

        //Initialization of all private properties
        public string Details
        {
            get
            {
                return this.details;
            }

            set
            {
                this.details = value;
                OnPropertyChanged(nameof(this.Details));
            }
        }

        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
        }

        public int SelectedDetailType
        {
            get
            {
                return this.selectedDetailType;
            }
            set
            {
                this.selectedDetailType = value;
                OnPropertyChanged(nameof(this.SelectedDetailType));
            }
        }

        public int SelectedContactId
        {
            get
            {
                return this.selectedContactId;
            }
            set
            {
                this.selectedContactId = value;
                OnPropertyChanged(nameof(this.SelectedContactId));
            }
        }
        #endregion

        #region Public Commands
        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new RelayCommand<object>(this.Save, this.CanSave);
                }

                return this.saveCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (this.clearCommand == null)
                {
                    this.clearCommand = new RelayCommand<object>(this.Clear);
                }

                return this.clearCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand<Window>(this.Close);
                }

                return this.closeCommand;
            }
        }
        #endregion

        #region Private methods
        //Can exdecute method for SaveCommand
        private bool CanSave(object args)
        {
            if (dbClass.RequiredField(Details))
            {

                return true;
            }
            else
            {
                errorMessage = "Field is required!";
                return false;
            }
        }

        //Save method
        private void Save(object args)
        {
            this.detailClass.ContactId = this.selectedContactId;
            this.detailClass.Value = this.details;
            this.detailClass.TypeId = this.selectedDetailType;

            if (SelectedDetailType == 4)
            {
                if (!dbClass.EmailValidator(Details))
                {
                    MessageBox.Show("Please enter correct e-mail!");
                    return;
                }
            }

            if (SelectedDetailType == 1 || SelectedDetailType == 2)
            {
                if (!dbClass.NumberValidator(Details))
                {
                    MessageBox.Show("Please enter correct number!");
                    return;
                }
            }

            bool IsSucces = this.dbClass.InsertIntoDetailsTable(this.detailClass);

            if (IsSucces)
            {
                MessageBox.Show("Succesfull added detail!");
                Clear(args);
            }
            else
            {
                MessageBox.Show("Can't add new detail!");
            }
        }

        //Clearing all TextBoxes when the button is clicked
        private void Clear(object args)
        {
            this.Details = string.Empty;
        }

        //Closing InsertWindow when the button is clicked
        private void Close(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }
        #endregion
    }
}