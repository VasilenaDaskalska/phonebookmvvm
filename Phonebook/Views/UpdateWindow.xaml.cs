﻿using Phonebook.ViewModels;
using System.Windows;

namespace Phonebook
{
    /// <summary>
    /// Interaction logic for UpdateWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        #region Constructor
        public UpdateWindow()
        {
            InitializeComponent();
            DataContext = new UpdateViewModel();
        }
        #endregion
    }
}

