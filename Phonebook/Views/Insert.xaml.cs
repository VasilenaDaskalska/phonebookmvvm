﻿using Phonebook.ViewModels;
using System.Windows;

namespace Phonebook
{
    /// <summary>
    /// Interaction logic for Insert.xaml
    /// </summary>
    public partial class Insert : Window
    {
        #region Constructor
        public Insert()
        {
            InitializeComponent();
            this.DataContext = new InsertViewModel();
        }
        #endregion
    }
}