﻿using Phonebook.ViewModels;
using System.Windows;

namespace Phonebook
{
    /// <summary>
    /// Interaction logic for MoreDetails.xaml
    /// </summary>
    public partial class MoreDetails : Window
    {
        #region Constructor
        public MoreDetails()
        {
            InitializeComponent();
            this.DataContext = new MoreDetailsViewModel();  
        }
        #endregion
    }
}
